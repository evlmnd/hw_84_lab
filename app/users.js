const express = require('express');
const User = require('../models/User');
const auth = require('../middleware/auth');


const router = express.Router();

router.post('/', (req, res) => {
    const data = req.body;

    const user = new User(data);
    user
        .save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send({error: error}));
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(400).send({error: 'Username/password not correct'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Username/password not correct'});
    }
    user.createToken();
    await user.save();

    return res.send({token: user.token});
});

router.get('/secret', auth, (req, res) => {
    res.send(req.user._id);
});

module.exports = router;