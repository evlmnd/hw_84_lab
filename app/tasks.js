const express = require('express');
const Task = require('../models/Task');
const auth = require('../middleware/auth');


const router = express.Router();

router.post('/', auth, (req, res) => {
    const data = req.body;

    const task = new Task(data);
    task
        .save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send({error: error}));
});

router.get('/', auth, async (req, res) => {
    Task.find({user: req.user._id})
        .then(result => res.send(result))
});

router.put('/:id', auth, async (req, res) => {
    const id = req.params.id;
    const data = req.body;
    const task = await Task.findOne({_id: id});

    if (!(req.user._id).equals(task.user)) {
        res.sendStatus(403);
    } else {
        task.set({title: data.title, description: data.description, status: data.status});
        task.save()
            .then(result => res.send(result));
    }
});

router.delete('/:id', auth, async (req, res) => {
    const id = req.params.id;
    try{
        const task = await Task.findOne({_id: id});
        if (!(req.user._id).equals(task.user)) {
            return res.sendStatus(403);
        } else {
            await Task.findOneAndDelete({_id: id});
            res.send({message: 'Removed!'})
        }
    } catch(e) {
        res.sendStatus(400);
    }
});

module.exports = router;